import { ApolloClient, InMemoryCache } from "@apollo/client";

const clientApollo = new ApolloClient({
  uri: "https://api-pokemon-graphql.herokuapp.com/",
  cache: new InMemoryCache(),
});

export default clientApollo;
