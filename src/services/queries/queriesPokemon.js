import { gql } from "@apollo/client";

export const POKEMON_LIST = gql`
  query {
    pokemons(offset: 0, first: 1000) {
      id
      name
      image
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      maxHP
      types
      weaknesses
      resistant
      attacks {
        fast {
          name
          type
          damage
        }
        special {
          name
          type
          damage
        }
      }
      evolutions {
        name
        image
      }
    }
  }
`;
