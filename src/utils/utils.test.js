import { subscribe } from "graphql";
import { returnImgTypes } from "./utils";

// Imagens
import Grass from "../imgs/grass.png";
import Poison from "../imgs/poison.png";
import Fire from "../imgs/fire.png";
import Flying from "../imgs/flying.png";
import Water from "../imgs/water.png";
import Bug from "../imgs/bug.png";
import Normal from "../imgs/normal.png";
import Electric from "../imgs/electric.png";
import Ground from "../imgs/ground.png";
import Fairy from "../imgs/fairy.png";
import Fighting from "../imgs/fighting.png";
import Psychic from "../imgs/psychic.png";
import Rock from "../imgs/rock.png";
import Steel from "../imgs/steel.png";
import Ice from "../imgs/ice.png";
import Ghost from "../imgs/ghost.png";
import Dragon from "../imgs/dragon.png";
import Interrogation from "../imgs/interrogation.png";

describe("Utils", () => {
  test("type Grass", () => expect(returnImgTypes("Grass")).toEqual(Grass));
  test("type Poison", () => expect(returnImgTypes("Poison")).toEqual(Poison));
  test("type Fire", () => expect(returnImgTypes("Fire")).toEqual(Fire));
  test("type Flying", () => expect(returnImgTypes("Flying")).toEqual(Flying));
  test("type Water", () => expect(returnImgTypes("Water")).toEqual(Water));
  test("type Bug", () => expect(returnImgTypes("Bug")).toEqual(Bug));
  test("type Normal", () => expect(returnImgTypes("Normal")).toEqual(Normal));
  test("type Electric", () =>
    expect(returnImgTypes("Electric")).toEqual(Electric));
  test("type Ground", () => expect(returnImgTypes("Ground")).toEqual(Ground));
  test("type Fairy", () => expect(returnImgTypes("Fairy")).toEqual(Fairy));
  test("type Fighting", () =>
    expect(returnImgTypes("Fighting")).toEqual(Fighting));
  test("type Psychic", () =>
    expect(returnImgTypes("Psychic")).toEqual(Psychic));
  test("type Rock", () => expect(returnImgTypes("Rock")).toEqual(Rock));
  test("type Steel", () => expect(returnImgTypes("Steel")).toEqual(Steel));
  test("type Ice", () => expect(returnImgTypes("Ice")).toEqual(Ice));
  test("type Ghost", () => expect(returnImgTypes("Ghost")).toEqual(Ghost));
  test("type Dragon", () => expect(returnImgTypes("Dragon")).toEqual(Dragon));
  test("type Interrogation", () =>
    expect(returnImgTypes("Interrogation")).toEqual(Interrogation));
});
