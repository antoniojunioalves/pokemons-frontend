// Imagens
import Grass from "../imgs/grass.png";
import Poison from "../imgs/poison.png";
import Fire from "../imgs/fire.png";
import Flying from "../imgs/flying.png";
import Water from "../imgs/water.png";
import Bug from "../imgs/bug.png";
import Normal from "../imgs/normal.png";
import Electric from "../imgs/electric.png";
import Ground from "../imgs/ground.png";
import Fairy from "../imgs/fairy.png";
import Fighting from "../imgs/fighting.png";
import Psychic from "../imgs/psychic.png";
import Rock from "../imgs/rock.png";
import Steel from "../imgs/steel.png";
import Ice from "../imgs/ice.png";
import Ghost from "../imgs/ghost.png";
import Dragon from "../imgs/dragon.png";
import Interrogation from "../imgs/interrogation.png";

const returnImgTypes = (type) => {
  switch (type) {
    case "Fire":
      return Fire;
    case "Grass":
      return Grass;
    case "Poison":
      return Poison;
    case "Water":
      return Water;
    case "Flying":
      return Flying;
    case "Bug":
      return Bug;
    case "Normal":
      return Normal;
    case "Electric":
      return Electric;
    case "Ground":
      return Ground;
    case "Fairy":
      return Fairy;
    case "Fighting":
      return Fighting;
    case "Psychic":
      return Psychic;
    case "Rock":
      return Rock;
    case "Steel":
      return Steel;
    case "Ice":
      return Ice;
    case "Ghost":
      return Ghost;
    case "Dragon":
      return Dragon;
    default:
      return Interrogation;
  }
};

export { returnImgTypes };
