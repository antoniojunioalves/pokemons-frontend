// External Libs
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

// Actions
import * as actions from "./actionPokemon";

const mockStore = configureMockStore([thunk]);

describe("Actions test", () => {
  let store;

  beforeEach(() => {
    store = mockStore({
      pokemons: {
        pokemonListOriginal: [],
        pokemonList: [],
        pokemonDetails: {},
        updateList: true,
      },
    });
  });

  test("Action setPokemonList", () => {
    const pokemons = {
      id: "UG9rZW1vbjowMDE=",
      name: "Bulbasaur",
      image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
    };

    const action = actions.setPokemonList(pokemons);

    expect(action).toEqual({
      type: "SET_POKEMON_LIST",
      payload: pokemons,
    });
  });

  test("Action setPokemonList", async () => {
    const pokemon = {
      id: "UG9rZW1vbjowMDE=",
      name: "Bulbasaur",
      image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
    };
    await store.dispatch(actions.setPokemonList(pokemon));
    expect(store.getActions()).toEqual([
      {
        payload: pokemon,
        type: "SET_POKEMON_LIST",
      },
    ]);
  });

  test("Action getPokemon", async () => {
    const idPokemon = "UG9rZW1vbjowMDE=";
    await store.dispatch(actions.getPokemon(idPokemon));

    expect(store.getActions()).toEqual([
      {
        type: "GET_POKEMON_DETAILS_SUCESS",
        payload: idPokemon,
      },
    ]);
  });

  test("Action updatePokemonList", async () => {
    const list = [
      {
        id: "UG9rZW1vbjowMDE=",
        name: "Bulbasaur",
        image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      },
      {
        id: "UG9rZW1vbjowMDI=",
        name: "Ivysaur",
        image: "https://img.pokemondb.net/artwork/Ivysaur.jpg",
      },
    ];
    await store.dispatch(actions.updatePokemonList(list));

    expect(store.getActions()).toEqual([
      {
        type: "UPDATE_POKEMON_LIST",
        payload: list,
      },
    ]);
  });

  test("Action updatePokemon", async () => {
    const pokemon = {
      id: "UG9rZW1vbjowMDE=",
      name: "Bulbasaur",
      image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
    };

    await store.dispatch(actions.updatePokemon(pokemon));

    expect(store.getActions()).toEqual([
      {
        type: "UPDATE_POKEMON",
        payload: pokemon,
      },
    ]);
  });
});
