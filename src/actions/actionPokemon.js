// Constants
import {
  SET_POKEMON_LIST,
  GET_POKEMON_DETAILS_SUCESS,
  UPDATE_POKEMON_LIST,
  UPDATE_POKEMON,
} from "../constants/reducersActions";

export const setPokemonList = (pokemons) => ({
  type: SET_POKEMON_LIST,
  payload: pokemons,
});

export const getPokemon = (idPokemon) => (dispatch) => {
  dispatch({
    type: GET_POKEMON_DETAILS_SUCESS,
    payload: idPokemon,
  });
};

export const updatePokemonList = (list) => (dispatch) => {
  dispatch({
    type: UPDATE_POKEMON_LIST,
    payload: list,
  });
};

export const updatePokemon = (pokemon) => (dispatch) => {
  dispatch({
    type: UPDATE_POKEMON,
    payload: pokemon,
  });
};
