// Constants
import {
  SET_POKEMON_LIST,
  GET_POKEMON_DETAILS_SUCESS,
  UPDATE_POKEMON_LIST,
  UPDATE_POKEMON,
} from "../constants/reducersActions";

const initialState = {
  pokemonListOriginal: [],
  pokemonList: [],
  pokemonDetails: {},
  updateList: true,
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case SET_POKEMON_LIST:
      return {
        ...state,
        updateList: false,
        pokemonList: action.payload,
        pokemonListOriginal: action.payload,
      };

    case GET_POKEMON_DETAILS_SUCESS:
      return {
        ...state,
        pokemonDetails: state.pokemonList.find(
          (pokemon) => pokemon.id === action.payload
        ),
      };

    case UPDATE_POKEMON_LIST:
      return {
        ...state,
        pokemonList: action.payload,
      };

    case UPDATE_POKEMON:
      return {
        ...state,
        pokemonList: state.pokemonList.map((pokemon) =>
          pokemon.id === action.payload.id ? action.payload : pokemon
        ),
        pokemonListOriginal: state.pokemonListOriginal.map((pokemon) =>
          pokemon.id === action.payload.id ? action.payload : pokemon
        ),
      };

    default:
      return state;
  }
};

export default reducers;
