import reducerPokemon from "./reducerPokemon";

describe("reducerPokemon", () => {
  const state = {
    pokemonListOriginal: [],
    pokemonList: [],
    pokemonDetails: {},
    updateList: true,
  };

  test("return default value store", () => {
    expect(reducerPokemon(undefined, {})).toEqual(state);
  });

  test("return when SET_POKEMON_LIST", () => {
    const action = { type: "SET_POKEMON_LIST", payload: [] };

    expect(reducerPokemon(undefined, action)).toEqual({
      ...state,
      updateList: false,
      pokemonList: action.payload,
      pokemonListOriginal: action.payload,
    });
  });

  test("return when GET_POKEMON_DETAILS_SUCESS", () => {
    const stateLocal = {
      ...state,
      pokemonDetails: {
        id: "UG9rZW1vbjowMDE=",
        name: "Bulbasaur",
        image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      },
      pokemonList: [
        {
          id: "UG9rZW1vbjowMDE=",
          name: "Bulbasaur",
          image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
        },
        {
          id: "UG9rZW1vbjowMDI=",
          name: "Ivysaur",
          image: "https://img.pokemondb.net/artwork/ivysaur.jpg",
        },
      ],
    };

    const action = {
      type: "GET_POKEMON_DETAILS_SUCESS",
      payload: "UG9rZW1vbjowMDE=",
    };

    expect(reducerPokemon(stateLocal, action)).toEqual({ ...stateLocal });
  });

  test("return when GET_POKEMON_LIST_REQUEST", () => {
    const action = { type: "GET_POKEMON_LIST_REQUEST" };
    const stateLocal = {
      ...state,
      loading: true,
    };
    expect(reducerPokemon(stateLocal, action)).toEqual(stateLocal);
  });

  test("return when GET_POKEMON_LIST_ERROR", () => {
    const action = { type: "GET_POKEMON_LIST_ERROR" };

    expect(reducerPokemon(undefined, action)).toEqual(state);
  });

  test("return when UPDATE_POKEMON_LIST", () => {
    const action = { type: "UPDATE_POKEMON_LIST", payload: [] };

    expect(reducerPokemon(undefined, action)).toEqual({
      ...state,
      pokemonListOriginal: action.payload,
    });
  });

  test("return when UPDATE_POKEMON", () => {
    const list = [
      {
        id: "UG9rZW1vbjowMDE=",
        name: "Bulbasaur",
        image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      },
    ];

    const stateLocal = {
      ...state,
      pokemonList: list,
      pokemonListOriginal: list,
    };

    const action = {
      type: "UPDATE_POKEMON",
      payload: {
        id: "UG9rZW1vbjowMDE=",
        name: "Bulbasaur",
        image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      },
    };

    expect(reducerPokemon(stateLocal, action)).toEqual({ ...stateLocal });
  });

  test("return when UPDATE_POKEMON modify pokemon", () => {
    const list = [
      {
        id: "UG9rZW1vbjowMDE=",
        name: "Bulbasaur",
        image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      },
    ];

    const pokemonModify = {
      id: "UG9rZW1vbjowMDE=",
      name: "TESTE",
      image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
    };

    const stateLocal = {
      ...state,
      pokemonList: list,
      pokemonListOriginal: list,
    };

    const action = {
      type: "UPDATE_POKEMON",
      payload: pokemonModify,
    };

    const stateLocalModify = {
      ...state,
      pokemonList: [pokemonModify],
      pokemonListOriginal: [pokemonModify],
    };

    expect(reducerPokemon(stateLocal, action)).toEqual({ ...stateLocalModify });
  });

  test("return when UPDATE_POKEMON not found pokemon", () => {
    const list = [
      {
        id: "UG9rZW1vbjowMDE=",
        name: "Bulbasaur",
        image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      },
    ];

    const pokemonModify = {
      id: "UG9rZW1vbjowMDI=",
      name: "Ivysaur",
      image: "https://img.pokemondb.net/artwork/ivysaur.jpg",
    };

    const stateLocal = {
      ...state,
      pokemonList: list,
      pokemonListOriginal: list,
    };

    const action = {
      type: "UPDATE_POKEMON",
      payload: pokemonModify,
    };

    expect(reducerPokemon(stateLocal, action)).toEqual({ ...stateLocal });
  });
});
