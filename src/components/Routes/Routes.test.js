// External libs
import { render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

// Component
import Routes from "./Routes";

describe("Component <Routes />", () => {
  const renderComponent = () => {
    const mockStore = configureMockStore([thunk]);

    const store = mockStore({
      pokemonList: [],
      pokemonCard: {},
    });

    return render(
      <Provider store={store}>
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </Provider>
    );
    expect(component).toBeDefined();
  };

  test("should render component", () => {
    const component = renderComponent();
    expect(component).toBeDefined();
  });

  test("render component default <PokemonList />", () => {
    const { container } = renderComponent();
    expect(
      container.querySelector("section[class=pokemonlist-container]")
    ).toBeInTheDocument();
  });
});
