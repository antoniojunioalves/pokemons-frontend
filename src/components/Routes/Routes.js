// External libs
import React from "react";
import { Switch, Route } from "react-router-dom";

// Components
import PokemonList from "../PokemonList";
import PokemonDetails from "../PokemonDetails";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={PokemonList} />
      <Route exact path="/pokemon/:idPokemon" component={PokemonDetails} />
      <Route exact path="*" component={PokemonList} />
    </Switch>
  );
};

export default Routes;
