// External Libs
import React from "react";

// CSS
import "./_inputDetails.scss";

const InputDetails = ({ title, value, edit, onChange, dataTestId }) => {
  return (
    <div className="input-details-container">
      <p>{title}</p>
      {edit ? (
        <input
          data-testid={dataTestId}
          value={value}
          onChange={({ target: { value } }) => onChange(value)}
        />
      ) : (
        <p data-testid="input-details-not-edit">{value}</p>
      )}
    </div>
  );
};

export default InputDetails;
