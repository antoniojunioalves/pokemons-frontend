// External libs
import { fireEvent, render } from "@testing-library/react";

// Component
import InputDetails from "./InputDetails";

describe("Component <InputDetails />", () => {
  test("should render component", () => {
    const component = render(<InputDetails />);
    expect(component).toBeDefined();
  });

  test("Value title", () => {
    const { getByText } = render(<InputDetails title="Title Teste" />);

    expect(getByText("Title Teste")).toBeInTheDocument();
  });

  test("not edit", () => {
    const { getByTestId } = render(<InputDetails edit={false} />);
    expect(getByTestId("input-details-not-edit")).toBeInTheDocument();
  });

  test("in edit", () => {
    const { getByTestId } = render(
      <InputDetails edit={true} dataTestId="inputEdit" />
    );

    expect(getByTestId("inputEdit")).toBeInTheDocument();
  });

  test("input value default", () => {
    const { container } = render(
      <InputDetails edit={true} dataTestId="inputEdit" value="ValueTeste" />
    );

    const input = container.querySelector("input[value=ValueTeste]");
    expect(input).toBeInTheDocument();
  });

  test("onChange in edit", () => {
    const onChange = jest.fn();

    const { getByTestId } = render(
      <InputDetails edit={true} dataTestId="inputEdit" onChange={onChange} />
    );

    const input = getByTestId("inputEdit");
    fireEvent.change(input, { target: { value: "ValueTeste" } });

    expect(input.value).toEqual("ValueTeste");
  });
});
