// External Libs
import React from "react";

// CSS
import "./_evolutions.scss";

// Image
import ArrowRight from "../../../imgs/arrow-right.png";
import ArrowDown from "../../../imgs/arrow-down.png";

const Evolutions = ({ evolutions }) => {
  const getImagemPokemon = (image, name, visibleArrow) => {
    return (
      <li
        data-testid={`evolutions-item-${name}`}
        key={name}
        className="evolutions-item"
      >
        <label>{name}</label>
        <img className="evolutions-img" src={image} />
        {visibleArrow ? (
          <>
            <img
              data-testid={`evolutions-arrow-right-${name}`}
              className="evolutions-arrow-right"
              src={ArrowRight}
            />
            <img
              data-testid={`evolutions-arrow-down-${name}`}
              className="evolutions-arrow-down"
              src={ArrowDown}
            />
          </>
        ) : null}
      </li>
    );
  };

  return evolutions ? (
    <div className="evolutions-container">
      <label>Evolutions</label>
      <ul className="evolutions-list">
        {evolutions?.map(({ image, name }, idx) => {
          return getImagemPokemon(
            image,
            name,
            !(evolutions.length === idx + 1)
          );
        })}
      </ul>
    </div>
  ) : null;
};

export default Evolutions;
