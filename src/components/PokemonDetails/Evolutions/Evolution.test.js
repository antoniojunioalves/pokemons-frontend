// External libs
import { render } from "@testing-library/react";

// Component
import Evolutions from "./Evolutions";

describe("Component <Evolutions />", () => {
  test("should render component", () => {
    const component = render(<Evolutions />);
    expect(component).toBeDefined();
  });

  test("single evolution without arrow", () => {
    const evolutions = [{ image: "venusaur.jpg", name: "Venusaur" }];

    const { getByTestId } = render(<Evolutions evolutions={evolutions} />);

    expect(getByTestId("evolutions-item-Venusaur")).toBeInTheDocument();
  });

  test("single evolution with arrow", () => {
    const evolutions = [
      { image: "venusaur.jpg", name: "Venusaur" },
      { image: "ivysaur.jpg", name: "Ivysaur" },
    ];

    const { getByTestId } = render(<Evolutions evolutions={evolutions} />);

    expect(getByTestId("evolutions-arrow-down-Venusaur")).toBeInTheDocument();
    expect(getByTestId("evolutions-arrow-right-Venusaur")).toBeInTheDocument();
  });
});
