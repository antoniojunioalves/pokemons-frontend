// External Libs
import React from "react";

// Utils
import { returnImgTypes } from "../../../utils/utils";

// CSS
import "./_attacks.scss";

const Attacks = ({ attacks }) => {
  const getAttacks = (attackCategory, attack) => {
    return (
      <div className="attacks-list">
        <p>{attackCategory}</p>
        {attack?.map(({ name, type, damage }) => (
          <div
            data-testid={`attacks-item-${name}`}
            key={name}
            className="attacks"
          >
            <img src={returnImgTypes(type)} />
            <p>{damage}</p>
            <p>{name}</p>
          </div>
        ))}
      </div>
    );
  };

  return attacks ? (
    <div className="attacks-container">
      <label>Attacks</label>
      {getAttacks("Fast", attacks?.fast)}
      {getAttacks("Especial", attacks?.special)}
    </div>
  ) : null;
};

export default Attacks;
