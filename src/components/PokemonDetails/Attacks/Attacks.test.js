// External libs
import { render } from "@testing-library/react";

// Component
import Attacks from "./Attacks";

describe("Component <Attacks />", () => {
  test("should render component", () => {
    const component = render(<Attacks />);
    expect(component).toBeDefined();
  });

  test("attacks fast", () => {
    const attacks = {
      fast: [
        {
          name: "Ember",
          type: "Fire",
          damage: 10,
        },
        {
          name: "Scratch",
          type: "Normal",
          damage: 6,
        },
      ],
    };

    const { getByTestId } = render(<Attacks attacks={attacks} />);

    expect(getByTestId("attacks-item-Ember")).toBeInTheDocument();
    expect(getByTestId("attacks-item-Scratch")).toBeInTheDocument();
  });

  test("attacks special", () => {
    const attacks = {
      fast: [
        {
          name: "Flamethrower",
          type: "Fire",
          damage: 55,
        },
      ],
    };

    const { getByTestId } = render(<Attacks attacks={attacks} />);

    expect(getByTestId("attacks-item-Flamethrower")).toBeInTheDocument();
  });
});
