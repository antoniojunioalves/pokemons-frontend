// External libs
import { render } from "@testing-library/react";

// Component
import TypesDetails from "./TypesDetails";

describe("Component <TypesDetails />", () => {
  test("should render component", () => {
    const component = render(<TypesDetails />);
    expect(component).toBeDefined();
  });

  test("with title", () => {
    const { getByText } = render(<TypesDetails title="TitleTest" />);

    expect(getByText("TitleTest")).toBeInTheDocument();
  });

  test("render with types", () => {
    const types = ["Fire", "Grass"];
    const { getByTestId } = render(<TypesDetails types={types} />);

    expect(getByTestId("types-details-item-Fire")).toBeInTheDocument();
    expect(getByTestId("types-details-item-Grass")).toBeInTheDocument();
  });
});
