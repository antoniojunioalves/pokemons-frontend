// utils
import { returnImgTypes } from "../../../utils/utils";

//CSS
import "./_typesDetails.scss";

const TypesDetails = ({ title, types }) => {
  return (
    <>
      <label className="type-details-title">{title}</label>
      <div className="type-details-container">
        {types
          ? types.map((type) => (
              <div
                data-testid={`types-details-item-${type}`}
                key={type}
                className="type-details"
              >
                <img src={returnImgTypes(type)} alt={type} />
                <p>{type}</p>
              </div>
            ))
          : null}
      </div>
    </>
  );
};

export default TypesDetails;
