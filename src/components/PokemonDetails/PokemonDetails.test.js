// External libs
import { fireEvent, render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

// Component
import PokemonDetails, {
  PokemonDetails as PokemonDetailsComponent,
} from "./PokemonDetails";

describe("Component <PokemonDetails />", () => {
  let props;

  beforeEach(() => {
    const pokemon = {
      id: "UG9rZW1vbjowMDE=",
      name: "Bulbasaur",
      image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      weight: {
        minimum: "6.04kg",
        maximum: "7.76kg",
      },
      height: {
        minimum: "0.61m",
        maximum: "0.79m",
      },
      maxHP: 1071,
      types: ["Grass", "Poison"],
      weaknesses: ["Fire", "Ice", "Flying", "Psychic"],
      resistant: ["Water", "Electric", "Grass", "Fighting", "Fairy"],
      attacks: {
        fast: [
          { name: "Tackle", type: "Normal", damage: 12 },
          { name: "Vine Whip", type: "Grass", damage: 7 },
        ],
        special: [
          { name: "Power Whip", type: "Grass", damage: 70 },
          { name: "Seed Bomb", type: "Grass", damage: 40 },
          { name: "Sludge Bomb", type: "Poison", damage: 55 },
        ],
      },
      evolutions: [
        {
          name: "Ivysaur",
          image: "https://img.pokemondb.net/artwork/ivysaur.jpg",
        },
        {
          name: "Venusaur",
          image: "https://img.pokemondb.net/artwork/venusaur.jpg",
        },
      ],
    };

    props = {
      updatePokemon: jest.fn(),
      getPokemon: jest.fn(() => pokemon),
      pokemonDetails: pokemon,
    };
  });

  test("should render component", () => {
    const mockStore = configureMockStore([thunk]);

    const store = mockStore({});

    const component = render(
      <Provider store={store}>
        <BrowserRouter>
          <PokemonDetails />
        </BrowserRouter>
      </Provider>
    );
    expect(component).toBeDefined();
  });

  const renderComponente = (propsComponent) => {
    return render(
      <BrowserRouter>
        <PokemonDetailsComponent {...propsComponent} />
      </BrowserRouter>
    );
  };

  const testInputEditSave = (dataTestId, value, objValidation) => {
    const updatePokemon = jest.fn();
    props = { ...props, updatePokemon };
    const { getByTestId } = renderComponente(props);

    const buttonEdit = getByTestId("pokemon-details-edit");
    fireEvent.click(buttonEdit);

    const input = getByTestId(dataTestId);
    fireEvent.change(input, { target: { value } });

    const buttonSave = getByTestId("pokemon-details-save");
    fireEvent.click(buttonSave);

    expect(input.value).toEqual(value);

    expect(updatePokemon).toBeCalledWith(objValidation);
  };

  test("change name", () => {
    const validationExpect = {
      ...props.pokemonDetails,
      name: "NomePokemon",
    };

    testInputEditSave("pokemon-details-name", "NomePokemon", validationExpect);
  });

  test("change weight minimum", () => {
    const validationExpect = {
      ...props.pokemonDetails,
      weight: {
        ...props.pokemonDetails.weight,
        minimum: "1kg",
      },
    };

    testInputEditSave("pokemon-details-weight-min", "1kg", validationExpect);
  });

  test("change weight maximum", () => {
    const validationExpect = {
      ...props.pokemonDetails,
      weight: {
        ...props.pokemonDetails.weight,
        maximum: "50kg",
      },
    };

    testInputEditSave("pokemon-details-weight-max", "50kg", validationExpect);
  });

  test("change height minimum", () => {
    const validationExpect = {
      ...props.pokemonDetails,
      height: {
        ...props.pokemonDetails.height,
        minimum: "0.5m",
      },
    };

    testInputEditSave("pokemon-details-height-min", "0.5m", validationExpect);
  });

  test("change height maximum", () => {
    const validationExpect = {
      ...props.pokemonDetails,
      height: {
        ...props.pokemonDetails.height,
        maximum: "8m",
      },
    };

    testInputEditSave("pokemon-details-height-max", "8m", validationExpect);
  });

  test("change height maximum and cancel", () => {
    const updatePokemon = jest.fn();
    props = { ...props, updatePokemon };
    const { getByTestId } = renderComponente(props);

    const buttonEdit = getByTestId("pokemon-details-edit");
    fireEvent.click(buttonEdit);

    const input = getByTestId("pokemon-details-max-hp");
    fireEvent.change(input, { target: { value: "1050" } });
    expect(input.value).toEqual("1050");

    const buttonCancel = getByTestId("pokemon-details-cancel");
    fireEvent.click(buttonCancel);

    const h2 = getByTestId("pokemon-details-name-not-edit");

    expect(h2).toBeInTheDocument();
  });
});
