// External libs
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

// Actions
import { getPokemon, updatePokemon } from "../../actions/actionPokemon";

// Components
import TypesDetails from "./TypesDetails";
import Attacks from "./Attacks";
import InputDetails from "./InputDetails";
import Evolutions from "./Evolutions";

// Image
import { ReactComponent as Edit } from "../../imgs/edit.svg";
import { ReactComponent as Save } from "../../imgs/save.svg";
import { ReactComponent as Cancel } from "../../imgs/cancel.svg";

// CSS
import "./_pokemonDetails.scss";

export const PokemonDetails = ({
  getPokemon,
  pokemonDetails,
  updatePokemon,
}) => {
  const { idPokemon } = useParams();
  const [edit, setEdit] = useState(false);
  const [pokemon, setPokemon] = useState({});

  // estlint-disable-next-line
  useEffect(() => getPokemon(idPokemon), []);

  // estlint-disable-next-line
  useEffect(() => !!pokemonDetails && setPokemon(pokemonDetails), [
    pokemonDetails,
  ]);

  const {
    image,
    name,
    weight,
    height,
    maxHP,
    types,
    weaknesses,
    resistant,
    attacks,
    evolutions,
  } = pokemon;

  const handleChangeMaxHP = (maxHP) => setPokemon({ ...pokemon, maxHP });
  const handleChangeName = (name) => setPokemon({ ...pokemon, name });

  const handleChangeWeightMin = (minimum) =>
    setPokemon({ ...pokemon, weight: { ...pokemon.weight, minimum } });

  const handleChangeWeightMax = (maximum) =>
    setPokemon({ ...pokemon, weight: { ...pokemon.weight, maximum } });

  const handleChangeHeightMin = (minimum) =>
    setPokemon({ ...pokemon, height: { ...pokemon.height, minimum } });

  const handleChangeHeightMax = (maximum) =>
    setPokemon({ ...pokemon, height: { ...pokemon.height, maximum } });

  return (
    <section className="pokemon-details-container">
      <div className="pokemon-details-main">
        <img className="pokemon-details-img" src={image} />
        <div className="pokemon-details-description">
          <div className="pokemon-details-title">
            {edit ? (
              <input
                data-testid="pokemon-details-name"
                className="pokemon-details-name"
                maxLength={14}
                value={name}
                onChange={({ target: { value } }) => handleChangeName(value)}
              />
            ) : (
              <h2 data-testid="pokemon-details-name-not-edit">{name}</h2>
            )}
            {edit ? (
              <div>
                <Save
                  data-testid="pokemon-details-save"
                  className="pokemon-details-edit"
                  onClick={() => {
                    updatePokemon(pokemon);
                    setEdit(false);
                  }}
                />
                <Cancel
                  data-testid="pokemon-details-cancel"
                  className="pokemon-details-edit"
                  onClick={() => {
                    setPokemon(pokemonDetails);
                    setEdit(false);
                  }}
                />
              </div>
            ) : (
              <Edit
                data-testid="pokemon-details-edit"
                className="pokemon-details-edit"
                onClick={() => setEdit(true)}
              />
            )}
          </div>
          <div className="pokemon-details-weight-height">
            <p>Weight</p>
            <InputDetails
              dataTestId="pokemon-details-weight-min"
              title="Min"
              value={weight?.minimum}
              edit={edit}
              onChange={handleChangeWeightMin}
            />
            <InputDetails
              dataTestId="pokemon-details-weight-max"
              title="Max"
              value={weight?.maximum}
              edit={edit}
              onChange={handleChangeWeightMax}
            />
          </div>
          <div className="pokemon-details-weight-height">
            <p>Height</p>
            <InputDetails
              dataTestId="pokemon-details-height-min"
              title="Min"
              value={height?.minimum}
              edit={edit}
              onChange={handleChangeHeightMin}
            />
            <InputDetails
              dataTestId="pokemon-details-height-max"
              title="Max"
              value={height?.maximum}
              edit={edit}
              onChange={handleChangeHeightMax}
            />
          </div>
          <InputDetails
            dataTestId="pokemon-details-max-hp"
            title="Max HP"
            value={maxHP}
            edit={edit}
            onChange={handleChangeMaxHP}
          />
          <TypesDetails title="Type" types={types} />
          <TypesDetails title="Weakenesses" types={weaknesses} />
          <TypesDetails title="Resistent" types={resistant} />
        </div>
      </div>
      <Attacks attacks={attacks} />
      <Evolutions evolutions={evolutions} />
    </section>
  );
};

const mapStateToProps = (state) => ({ pokemonDetails: state.pokemonDetails });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ getPokemon, updatePokemon }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PokemonDetails);
