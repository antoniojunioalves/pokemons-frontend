// External libs
import React from "react";

// Components
import Header from "../Header";
import Routes from "../Routes";

// CSS
import "./_app.scss";

const App = () => {
  return (
    <main>
      <Header />
      <section className="app-routes">
        <Routes />
      </section>
    </main>
  );
};

export default App;
