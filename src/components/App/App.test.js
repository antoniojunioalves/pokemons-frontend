// External libs
import { render } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

// Component
import App from "./App";

describe("Component <App />", () => {
  test("should render component", () => {
    const mockStore = configureMockStore([thunk]);

    const store = mockStore({
      pokemonList: [],
      pokemonCard: {},
    });

    const component = render(
      <Provider store={store}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </Provider>
    );
    expect(component).toBeDefined();
  });
});
