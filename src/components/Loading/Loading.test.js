// External libs
import { render } from "@testing-library/react";

// Component
import Loading from "./Loading";

describe("Component <Loading />", () => {
  test("should render component", () => {
    const component = render(<Loading />);
    expect(component).toBeDefined();
  });

  test("Loading visible", () => {
    const { getByTestId } = render(<Loading loading={true} />);
    expect(getByTestId("loading-container")).toBeInTheDocument();
  });

  test("Loading with message", () => {
    const { getByText } = render(
      <Loading loading={true} message="TesteMenssage" />
    );
    expect(getByText("TesteMenssage")).toBeInTheDocument();
  });
});
