import React from "react";

import Spinner from "react-spinkit";

import "./_loading.scss";

const Loading = ({ loading, message }) => {
  return loading ? (
    <div data-testid="loading-container" className="loading-container">
      <div>
        <Spinner name="pacman" fadeIn="none" color="yellow" />
        <span className="loading-message">{message}</span>
      </div>
    </div>
  ) : null;
};

export default Loading;
