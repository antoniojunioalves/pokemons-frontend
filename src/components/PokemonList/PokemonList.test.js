// External libs
import { fireEvent, render, waitFor } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { MockedProvider } from "@apollo/client/testing";

// Component
import PokemonList, {
  PokemonList as PokemonListComponent,
} from "./PokemonList";

// queries
import { POKEMON_LIST } from "../../services/queries/queriesPokemon";

describe("Component <PokemonList />", () => {
  let props;

  beforeEach(() => {
    props = {
      pokemonList: [],
      pokemonListOriginal: [],
      updateList: false,
      updatePokemonList: jest.fn(),
      setPokemonList: jest.fn(),
    };
  });

  test("should render component", () => {
    const mockStore = configureMockStore([thunk]);
    const store = mockStore({ ...props });

    const component = render(
      <Provider store={store}>
        <BrowserRouter>
          <PokemonList />
        </BrowserRouter>
      </Provider>
    );

    expect(component).toBeDefined();
  });

  const renderComponente = () => {
    const mocks = [
      {
        request: {
          query: POKEMON_LIST,
        },
        result: {
          data: [
            {
              name: "Pokemon",
            },
          ],
        },
      },
    ];

    return render(
      <BrowserRouter>
        <MockedProvider mocks={mocks} addTypename={false}>
          <PokemonListComponent {...props} />
        </MockedProvider>
      </BrowserRouter>
    );
  };

  test("updateList true", async () => {
    const setPokemonList = jest.fn();

    props = {
      ...props,
      updateList: true,
      setPokemonList,
    };

    const { getByTestId } = renderComponente();
    await waitFor(() => {
      expect(getByTestId("pokemonlist-list")).toBeInTheDocument();
      // TODO: Mock do GraphQL não funcionou, o data não vem preenchido no useLazyQuery
      // expect(setPokemonList).toBeCalledWith({});
    });
  });

  test("mount list", () => {
    props = {
      ...props,
      pokemonList: [
        {
          id: "UG9rZW1vbjowMDE=",
          name: "Bulbasaur",
          image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
        },
        {
          id: "UG9rZW1vbjowMDI=",
          name: "Ivysaur",
          image: "https://img.pokemondb.net/artwork/ivysaur.jpg",
        },
      ],
    };

    const { getByTestId } = renderComponente();
    expect(
      getByTestId("pokemon-card-container-UG9rZW1vbjowMDE=")
    ).toBeInTheDocument(true);
  });

  test("search pokemon", async () => {
    const updatePokemonList = jest.fn();
    const list = [
      {
        id: "UG9rZW1vbjowMDE=",
        name: "Bulbasaur",
        image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      },
      {
        id: "UG9rZW1vbjowMDI==",
        name: "Ivysaur",
        image: "https://img.pokemondb.net/artwork/ivysaur.jpg",
      },
    ];

    props = {
      ...props,
      updateList: true,
      updatePokemonList,
      pokemonList: list,
      pokemonListOriginal: list,
    };

    const { getByTestId } = renderComponente();
    const input = getByTestId("input-search");
    fireEvent.change(input, { target: { value: "Bulb" } });
    fireEvent.keyPress(input, { charCode: 13 });

    await waitFor(() => {
      expect(updatePokemonList).toBeCalledWith([
        {
          id: "UG9rZW1vbjowMDE=",
          name: "Bulbasaur",
          image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
        },
      ]);
    });
  });

  test("Search for pokemon that is not on the list", () => {
    const updatePokemonList = jest.fn();
    const list = [
      {
        id: "UG9rZW1vbjowMDE=",
        name: "Bulbasaur",
        image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      },
    ];

    props = {
      ...props,
      updatePokemonList,
      pokemonList: list,
      pokemonListOriginal: list,
    };

    const { getByTestId } = renderComponente();
    const input = getByTestId("input-search");
    fireEvent.change(input, { target: { value: "TESTE" } });
    fireEvent.keyPress(input, { charCode: 13 });
    expect(updatePokemonList).toHaveBeenCalledWith([]);
  });

  test("Search for pokemon input is empty return complete list", () => {
    const updatePokemonList = jest.fn();
    const list = [
      {
        id: "UG9rZW1vbjowMDE=",
        name: "Bulbasaur",
        image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      },
    ];

    props = {
      ...props,
      updatePokemonList,
      pokemonList: list,
      pokemonListOriginal: list,
    };

    const { getByTestId } = renderComponente();
    const input = getByTestId("input-search");
    fireEvent.keyPress(input, { charCode: 13 });
    expect(updatePokemonList).toHaveBeenCalledWith(list);
  });

  test("inputSearch onClick image", () => {
    const updatePokemonList = jest.fn();

    props = {
      ...props,
      updatePokemonList,
      pokemonListOriginal: [
        {
          id: "UG9rZW1vbjowMDE=",
          name: "Bulbasaur",
          image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
        },
      ],
    };

    const { getByTestId } = renderComponente();

    const input = getByTestId("input-search");
    fireEvent.change(input, { target: { value: "Bulbasaur" } });

    const icon = getByTestId("icon-search");
    fireEvent.click(icon);
    expect(updatePokemonList).toBeCalledWith([
      {
        id: "UG9rZW1vbjowMDE=",
        name: "Bulbasaur",
        image: "https://img.pokemondb.net/artwork/bulbasaur.jpg",
      },
    ]);
  });
});
