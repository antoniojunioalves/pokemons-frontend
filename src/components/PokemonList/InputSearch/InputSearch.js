// Libs externas
import React, { useState } from "react";

// CSS
import "./_inputSearch.scss";

// Image
import { ReactComponent as Search } from "../../../imgs/search.svg";

const InputSearch = ({ onSearch }) => {
  const [value, setValue] = useState("");

  const handleKeyPress = (e) => e?.charCode === 13 && onSearch(value);

  return (
    <div className="input-search-container">
      <input
        data-testid="input-search"
        placeholder="Search"
        value={value}
        onKeyPress={(e) => {
          handleKeyPress(e);
        }}
        onChange={({ target: { value } }) => setValue(value)}
      />
      <Search data-testid="icon-search" onClick={() => onSearch(value)} />
    </div>
  );
};

export default InputSearch;
