// External libs
import React, { useEffect } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { useLazyQuery, useQuery } from "@apollo/client";

// Actions
import { updatePokemonList, setPokemonList } from "../../actions/actionPokemon";
import { POKEMON_LIST } from "../../services/queries/queriesPokemon";

// Components
import PokemonCard from "../PokemonCard";
import Loading from "../Loading";

// Components children
import InputSearch from "./InputSearch";

// CSS
import "./_pokemonList.scss";

export const PokemonList = ({
  pokemonList,
  pokemonListOriginal,
  updateList,
  updatePokemonList,
  setPokemonList,
}) => {
  const [getPokemon, { loading, error, data }] = useLazyQuery(POKEMON_LIST);

  // TODO: Criar um componente para mostrar o erro para o usuário.
  useEffect(() => console.log(error), [error]);

  useEffect(() => data && setPokemonList(data?.pokemons), [data]);

  useEffect(() => updateList && !!pokemonList && getPokemon(), []);

  const handleSearch = (text) => {
    const list = !!text
      ? pokemonListOriginal.filter((pokemon) =>
          pokemon.name.toUpperCase().indexOf(text.toUpperCase()) >= 0
            ? pokemon
            : null
        )
      : pokemonListOriginal;

    updatePokemonList(list);
  };

  return (
    <section className="pokemonlist-container">
      <Loading loading={loading} message="Loading..." />
      <InputSearch onSearch={handleSearch} />
      <ul data-testid="pokemonlist-list" className="pokemonlist-list">
        {pokemonList?.map((pokemon) => (
          <PokemonCard key={pokemon?.id} pokemon={pokemon} />
        ))}
      </ul>
    </section>
  );
};

const mapStateToProps = (state) => ({
  pokemonList: state.pokemonList,
  pokemonListOriginal: state.pokemonListOriginal,
  updateList: state.updateList,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ updatePokemonList, setPokemonList }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PokemonList);
