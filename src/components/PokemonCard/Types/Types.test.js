// External libs
import { render } from "@testing-library/react";

// Component
import Types from "./Types";

describe("Component <Types />", () => {
  test("should render component", () => {
    const component = render(<Types />);
    expect(component).toBeDefined();
  });

  test("render with types", () => {
    const types = ["Fire", "Grass"];
    const { getByTestId } = render(<Types types={types} />);

    expect(getByTestId("types-item-Fire")).toBeInTheDocument();
    expect(getByTestId("types-item-Grass")).toBeInTheDocument();
  });
});
