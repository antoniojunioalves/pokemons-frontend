// utils
import { returnImgTypes } from "../../../utils/utils";

// CSS
import "./_types.scss";

const Types = ({ types }) => {
  return (
    <div className="types-container">
      {types
        ? types.map((type) => (
            <div
              data-testid={`types-item-${type}`}
              key={type}
              className="types"
            >
              <img src={returnImgTypes(type)} alt={type} />
              <p>{type}</p>
            </div>
          ))
        : null}
    </div>
  );
};

export default Types;
