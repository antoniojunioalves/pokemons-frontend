// External libs
import { fireEvent, render } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";

// Component
import PokemonCard from "./PokemonCard";

describe("Component <PokemonCard />", () => {
  let pokemonLocal;
  let history;

  beforeEach(() => {
    pokemonLocal = {
      id: "UG9rZW1vbjowMDE=",
      name: "Bulbasaur",
      image: "",
      types: ["Fire", "Water"],
    };

    history = createMemoryHistory();
  });

  const renderComponent = (pokemon) => {
    return render(
      <Router history={history}>
        <PokemonCard pokemon={pokemon} />
      </Router>
    );
  };

  test("should render component", () => {
    const component = renderComponent(pokemonLocal);
    expect(component).toBeDefined();
  });

  test("should render component without types ", () => {
    const pokemon = {
      ...pokemonLocal,
      types: null,
    };

    const { getByTestId } = renderComponent(pokemon);
    expect(
      getByTestId("pokemon-card-container-UG9rZW1vbjowMDE=")
    ).toBeInTheDocument();
  });

  test("click Card", () => {
    const { getByTestId } = renderComponent(pokemonLocal);
    const card = getByTestId("pokemon-card-container-UG9rZW1vbjowMDE=");
    fireEvent.click(card);
    expect(history.location.pathname).toEqual("/pokemon/UG9rZW1vbjowMDE=");
  });
});
