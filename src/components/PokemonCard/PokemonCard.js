// External libs
import React from "react";
import { useHistory } from "react-router-dom";

// Components
import Types from "./Types";

// CSS
import "./_pokemonCard.scss";

const PokemonCard = ({ pokemon }) => {
  const history = useHistory();

  const hadleClickDetails = (id) => history.push(`pokemon/${id}`);

  return (
    <li
      data-testid={`pokemon-card-container-${pokemon?.id}`}
      className="pokemon-card-container"
      key={pokemon?.id}
      onClick={() => hadleClickDetails(pokemon?.id)}
    >
      <h2>{pokemon?.name}</h2>
      <img className="pokemon-card-img" src={pokemon?.image} />
      <Types types={pokemon?.types} />
    </li>
  );
};

export default PokemonCard;
