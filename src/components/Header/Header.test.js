// External libs
import { fireEvent, render } from "@testing-library/react";
import { createMemoryHistory } from "history";
import { Router } from "react-router-dom";

// Component
import Header from "./Header";

describe("Component <Header />", () => {
  const renderComponent = (history) => {
    return render(
      <Router history={history}>
        <Header />
      </Router>
    );
  };

  test("should render component", () => {
    const history = createMemoryHistory();
    const component = renderComponent(history);
    expect(component).toBeDefined();
  });

  test("click logo", () => {
    const history = createMemoryHistory();
    const { getByTestId } = renderComponent(history);

    const logo = getByTestId("header-logo");
    fireEvent.click(logo);
    expect(history.location.pathname).toEqual("/");
  });
});
