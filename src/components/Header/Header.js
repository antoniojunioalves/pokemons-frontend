// External libs
import React from "react";
import { useHistory } from "react-router-dom";

// Imagens
import logoPokemon from "../../imgs/logo-pokemon2.png";

// CSS
import "./_header.scss";

const Header = () => {
  const history = useHistory();

  return (
    <>
      <header className="header-container">
        <img
          data-testid="header-logo"
          onClick={() => history.push("/charizard")}
          className="header-logo"
          src={logoPokemon}
          alt="Visita"
        />
      </header>
      <hr />
    </>
  );
};

export default Header;
