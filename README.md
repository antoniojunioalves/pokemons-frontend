# `Lista de Pokémons`

Status do Projeto: Concluído :heavy_check_mark:

## `Descrição:`

Listar os Pokémons e monstrar as suas informações, efetuar buscas pelos Pokémons e nessa lista será possível verificar os nomes e tipos e também os detalhes tais como o nome, o tipo as forças, fraquesas e quais evoluções existe do pokemon selecionado.

> [Live Demo](https://pokemons-front.herokuapp.com/)

### `Como rodar a aplicação` :arrow_forward:

```bash
git clone https://gitlab.com/antoniojunioalves/pokemons-frontend.git
cd pokemons-frontend
yarn
yarn start
```

Abrir a Url [http://localhost:3000](http://localhost:3000) para visualizar o projeto.

### `Como rodar os testes` :pencil:

```bash
cd pokemons-frontend
yarn
yarn test
```

> Obs.: Para executar o teste com cobertura de código basta rodar o comando **yarn coverage**

### `Como criar o build para produção` :hammer:

```bash
cd pokemons-frontend
yarn
yarn build
```

### `Dependências`

| Dependences                                         |                                                          |                                              |
| --------------------------------------------------- | -------------------------------------------------------- | -------------------------------------------- |
| :small_blue_diamond: apollo/client: 3.3.6           | :small_blue_diamond: testing-library/jest-dom: 5.11.4    | :small_blue_diamond: node-fetch: 2.6.1       |
| :small_blue_diamond: testing-library/react: 11.1.0  | :small_blue_diamond: testing-library/user-event: 12.1.10 | :small_blue_diamond: fetch-mock: 9.11.0      |
| :small_blue_diamond: babel-eslint: 10.1.0           | :small_blue_diamond: eslint: 7.14.0                      | :small_blue_diamond: sass: 1.30.0            |
| :small_blue_diamond: eslint-config-prettier: 6.15.0 | :small_blue_diamond: eslint-plugin-prettier: 3.1.4       | :small_blue_diamond: redux-thunk: 2.3.0      |
| :small_blue_diamond: eslint-plugin-react: 7.21.5    | :small_blue_diamond: graphql: 15.4.0                     | :small_blue_diamond: redux-mock-store: 1.5.4 |
| :small_blue_diamond: prettier: 2.2.0                | :small_blue_diamond: react: 17.0.1                       | :small_blue_diamond: redux: 4.0.4            |
| :small_blue_diamond: react-dom: 17.0.1              | :small_blue_diamond: react-redux: 7.1.0                  | :small_blue_diamond: react-scripts: 4.0.1    |
| :small_blue_diamond: react-spinkit: 3.0.0           |                                                          |                                              |
